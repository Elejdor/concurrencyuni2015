#pragma once
#include "../utils/inputContext.h"

class CInputContext : public IInputContext
{
private:
	void ButtonUp( SInputInfo inputInfo );

public:
	virtual void ProcessInput( const SInputInfo& inputInfo ) override;
};
