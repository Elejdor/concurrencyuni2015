#pragma once

#include <queue>
#include <thread>


class CResource;
class CJobManager;

class CTaskManager
{
private:
	CResource*		m_resourceA;
	CResource*		m_resourceB;
	CJobManager*	m_jobManager;


public:
	CTaskManager();
	~CTaskManager();

	void OperateResourceA();
	void OperateResourceB();
	void OperateResourceAB();

private:
	void AsyncOperateResourceA();
	void AsyncOperateResourceB();
	void AsyncOperateResourceAB();
};


