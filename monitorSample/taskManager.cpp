#include "pch.h"
#include "taskManager.h"
#include "monitorApp.h"
#include "resource.h"

#include "../utils/debug.h"
#include "../concurrentApp/jobManager.h"

CTaskManager::CTaskManager()
{
	m_resourceA = new CResource;
	m_resourceB = new CResource;

	m_jobManager = new CJobManager;
}

CTaskManager::~CTaskManager()
{
	delete m_jobManager;
	m_jobManager = nullptr;

	delete m_resourceB;
	m_resourceB = nullptr;

	delete m_resourceA;
	m_resourceA = nullptr;
}

void CTaskManager::OperateResourceA()
{
	m_jobManager->AddAsyncMethod( this, &CTaskManager::AsyncOperateResourceA );
}

void CTaskManager::OperateResourceB()
{
	m_jobManager->AddAsyncMethod( this, &CTaskManager::AsyncOperateResourceB );
}

void CTaskManager::OperateResourceAB()
{
	m_jobManager->AddAsyncMethod( this, &CTaskManager::AsyncOperateResourceAB );
}

void CTaskManager::AsyncOperateResourceA()
{
	SpinLockGuard lock( *m_resourceA );
	DEBUG_INFO( "\nSingle lock %i\n", m_resourceA->GetId() );
	m_resourceA->Operate();
}

void CTaskManager::AsyncOperateResourceB()
{
	SpinLockGuard lock( *m_resourceB );
	DEBUG_INFO( "\nSingle lock %i\n", m_resourceB->GetId() );
	m_resourceB->Operate();
}

void CTaskManager::AsyncOperateResourceAB()
{
	SpinLockGuard lockA( *m_resourceA );
	DEBUG_INFO( "\nDouble lock %i\n", m_resourceA->GetId() );

	SpinLockGuard lockB( *m_resourceB );
	DEBUG_INFO( "Double lock %i\n", m_resourceB->GetId() );

	m_resourceA->Operate();
	m_resourceB->Operate();
}