#include "pch.h"
#include "monitorApp.h"

CApp* CreateApplication()
{
	GMyApplication = new CMyApp;
	return GMyApplication;
}

int main()
{
	GApplication->Init();
	GApplication->MainLoop();
	GApplication->Shutdown();

	return 0;
}