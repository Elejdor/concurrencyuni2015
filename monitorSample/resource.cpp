#include "pch.h"
#include "resource.h"
#include "../utils/debug.h"
#include <windows.h>

Byte CResource::m_count = 0;

CResource::CResource()
	: m_id( m_count++ )
	, m_operationCount( 0 )
{

}

void CResource::Operate()
{
	DEBUG_INFO( "Resource %i started %i\n", m_id, m_operationCount.load() );
	Sleep( 2000 );
	DEBUG_INFO( "Resource %i finished %i\n", m_id, m_operationCount.load() );
	++m_operationCount;
}
