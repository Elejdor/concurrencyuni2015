#pragma once
#include "../concurrentApp/spinLockMutex.h"

class CResource : public CSpinLockMutex
{
private:
	static Byte m_count;
	Byte m_id;
	std::atomic<Byte> m_operationCount;

public:
	CResource();
	void Operate();
	GF_INLINE Byte GetId() { return m_id; }
};