#pragma once
#include "../concurrentApp/application.h"

class CResource;
class CJobManager;
class CTaskManager;

class CMyApp : public CApp
{
private:
	CResource*		m_resource0;
	CResource*		m_resource1;
	CTaskManager*	m_taskManager;

public:
	virtual void Init() override;
	virtual void Shutdown() override;

	GF_INLINE CTaskManager* const GetTaskManager() const { return m_taskManager; }

protected:
	virtual void Tick() override;
};

extern CMyApp* GMyApplication;