#include "pch.h"
#include "input.h"

#include "monitorApp.h"
#include "taskManager.h"
#include "resource.h"

#include "../utils/inputCodes.h"

void CInputContext::ButtonUp( SInputInfo inputInfo )
{
	if ( inputInfo.x == RI_ESCAPE )
	{
		GApplication->RequestQuit();
	}
	else if ( inputInfo.x == RI_F1 )
	{
		GMyApplication->GetTaskManager()->OperateResourceA();
	}
	else if ( inputInfo.x == RI_F2 )
	{
		GMyApplication->GetTaskManager()->OperateResourceB();
	}
	else if ( inputInfo.x == RI_F3 )
	{
		GMyApplication->GetTaskManager()->OperateResourceAB();
	}
}

void CInputContext::ProcessInput( const SInputInfo& inputInfo )
{
	if ( inputInfo.eventType == ERIEventType::RIE_UP )
	{
		ButtonUp( inputInfo );
	}
}
