#include "pch.h"
#include "monitorApp.h"

#include "input.h"
#include "resource.h"
#include "taskManager.h"

#include "../utils/inputCodes.h"
#include "../utils/rawInputManager.h"

CMyApp* GMyApplication;

void CMyApp::Init()
{
	m_inputContext = new CInputContext;
	m_taskManager = new CTaskManager;

	CApp::Init();
}

void CMyApp::Tick()
{

}

void CMyApp::Shutdown()
{
	CApp::Shutdown();

	delete m_taskManager;
	m_taskManager = nullptr;

	delete m_resource1;
	m_resource1 = nullptr;

	delete m_resource0;
	m_resource0 = nullptr;

	delete m_inputContext;
	m_inputContext = nullptr;
}
