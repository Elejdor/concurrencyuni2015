#pragma once

class CClient
{
private:
	struct addrinfo *result, *ptr;
	SOCKET ConnectSocket;

public:
	CClient();
	~CClient();
	Int32 Run();
	
private:
	void Receive();
	void Send( Byte numOp, Byte numRes );

	Int32 CreateClientSocket();
	Int32 ConnectToServer();
	Int32 SendAndReceive();
};