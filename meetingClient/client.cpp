#include "pch.h"
#include "client.h"

CClient::CClient() 
	: result( nullptr )
	, ptr( nullptr )
{
}

CClient::~CClient()
{
	shutdown( ConnectSocket, SD_SEND );
	closesocket( ConnectSocket );
	WSACleanup();
}

Int32 CClient::Run()
{
	ERROR_CHECK( InitializeWinsoc() );
	ERROR_CHECK( CreateClientSocket() );
	ERROR_CHECK( ConnectToServer() );
	ERROR_CHECK( SendAndReceive() );

	return 0;
}

void CClient::Receive()
{
	const Int32 recvbuflen = sizeof( SResponse );
	char recvbuf[recvbuflen];

	recv( ConnectSocket, recvbuf, recvbuflen, 0 );
	SResponse* response = reinterpret_cast< SResponse* >( &recvbuf );
	printf( "Response: %d\n", response->m_data );
}

void CClient::Send( Byte numOp, Byte numRes )
{
	SQuery* sendbuf = new SQuery;
	sendbuf->m_operationFlag = (EOperations)numOp;
	sendbuf->m_resourceNum = numRes;

	// Send an initial buffer
	send( ConnectSocket, ( char* )( sendbuf ), sizeof( SQuery ), 0 );
	delete sendbuf;
}

Int32 CClient::CreateClientSocket()
{
	result = NULL;
	ptr = NULL;
	addrinfo hints;

	ZeroMemory( &hints, sizeof( hints ) );
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	Int32 errCode;

	// Resolve the server address and port
	errCode = getaddrinfo( "127.0.0.1", DEFAULT_PORT, &hints, &result );
	if ( errCode != 0 ) {
		printf( "getaddrinfo failed: %d\n", errCode );
		WSACleanup();
		return 1;
	}

	ConnectSocket = INVALID_SOCKET;

	// Attempt to connect to the first address returned by
	// the call to getaddrinfo
	ptr = result;

	// Create a SOCKET for connecting to server
	ConnectSocket = socket( ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol );

	if ( ConnectSocket == INVALID_SOCKET ) {
		printf( "Error at socket(): %ld\n", WSAGetLastError() );
		freeaddrinfo( result );
		WSACleanup();
		return 1;
	}

	return 0;
}

Int32 CClient::ConnectToServer()
{
	Int32 errorCode;

	// Connect to server.
	errorCode = connect( ConnectSocket, ptr->ai_addr, ( int )ptr->ai_addrlen );
	if ( errorCode == SOCKET_ERROR ) {
		closesocket( ConnectSocket );
		ConnectSocket = INVALID_SOCKET;
	}

	freeaddrinfo( result );

	if ( ConnectSocket == INVALID_SOCKET ) {
		printf( "Unable to connect to server!\n" );
		WSACleanup();
		return 1;
	}

	return 0;
}

#define CONN_ERR(x) \
if ( x == SOCKET_ERROR ) { \
	printf( "shutdown failed: %d\n", WSAGetLastError() ); \
	closesocket( ConnectSocket ); \
	WSACleanup(); \
	return 1; }

Int32 CClient::SendAndReceive()
{
	Send( EOperations::RandByte, FLAG( 0 ) );
	Receive();

	Send( EOperations::RandBool, FLAG( 1 ) );
	Receive();

	Send( EOperations::RandByte, FLAG( 0 ) | FLAG( 1 ) );
	Receive();

	shutdown( ConnectSocket, SD_SEND );
	WSACleanup();

	return 0;
}
