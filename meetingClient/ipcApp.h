#pragma once
#include "../concurrentApp/application.h"

class CIpcApp : public CApp
{
public:

	virtual void Init() override;
	virtual void Tick() override;
	virtual void Shutdown() override;
};

extern CIpcApp* GIpcApplication;