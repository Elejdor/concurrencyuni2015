/*
** All copyrights reserved by GridFury @ 2015 **
*/

#pragma once

typedef long long Int64;
typedef unsigned long long UInt64;

typedef int Int32;
typedef unsigned int Uint32;
typedef int Int;
typedef Uint32 UInt;

typedef short Int16;
typedef unsigned short UInt16;

typedef char Int8;
typedef unsigned char UInt8;

typedef bool Bool;
typedef unsigned char Byte;

typedef double Double;

typedef char Char;
typedef float Float;

typedef unsigned char Byte;