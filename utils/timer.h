/*
** All copyrights reserved by GridFury @ 2015 **
*/

#pragma once
#include <chrono>

class CTimer
{
private:
	typedef std::chrono::high_resolution_clock::time_point TimePoint;
	typedef std::chrono::high_resolution_clock ChronoClock;
	typedef std::chrono::duration<double> HRDuration;

	HRDuration m_tickDuration;
	HRDuration m_duration;
	Double m_timeScale;
	
	TimePoint m_newTick;
	TimePoint m_oldTick;

public:
	CTimer();

	void Reset();
	void Tick();
	
	GF_INLINE Double GetTime() const { return m_duration.count(); }
	GF_INLINE Double GetDeltaTime() const { return m_tickDuration.count(); }

	GF_INLINE void SetTimeScale( Double value ) { m_timeScale = ( value >= 0 ) ? value : 0; }
};