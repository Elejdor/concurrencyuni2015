/*
** All copyrights reserved by GridFury @ 2015 **
*/

#include "pch.h"
#include "debug.h"

#include <windows.h>

namespace Debug
{
	void ShowMessageBox( Char* message, Char* title )
	{
		MessageBox(
			NULL,
			title,
			message,
			MB_ICONEXCLAMATION | MB_DEFBUTTON2
			);
	}
}