/*
** All copyrights reserved by GridFury @ 2015 **
*/

#pragma once

class CWindow
{
	friend class CEngine;

private:
	void* m_hInstance;
	void* m_hWnd;

	Int32 m_width = 800;
	Int32 m_height = 600;

public:
	bool Create();

	GF_INLINE void* GetHwnd() const { return m_hWnd; }
};