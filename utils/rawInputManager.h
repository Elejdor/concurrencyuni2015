/*
** All copyrights reserved by GridFury @ 2015 **
*/

#pragma once

#include "inputCodes.h"
#include "inputContext.h"

class CRawInputManager
{
private:
	IInputContext* m_currentInputContext;
	SInputInfo m_inputInfo;

public:
	CRawInputManager();
	~CRawInputManager();

	GF_INLINE void SetInputContext( IInputContext* const inputContext ) { m_currentInputContext = inputContext; }
	void InputProc( Uint32 message, Uint32 parameter );
};