#include "pch.h"
#include "connection.h"

Int32 InitializeWinsoc()
{
	WSADATA wsaData;

	Int32 errCode;

	// Initialize Winsock
	errCode = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (errCode != 0) {
		printf("WSAStartup failed: %d\n", errCode);
		return 1;
	}

	return 0;
}
