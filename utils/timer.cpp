/*
** All copyrights reserved by GridFury @ 2015 **
*/

#include "pch.h"
#include "timer.h"

CTimer::CTimer() 
	: m_timeScale( 1 )
{

}

void CTimer::Reset()
{
	m_oldTick = ChronoClock::now();
}

void CTimer::Tick()
{
	m_newTick = ChronoClock::now();

	m_tickDuration = std::chrono::duration_cast< HRDuration > ( m_newTick - m_oldTick ) * m_timeScale; //since previous frame
	m_oldTick = m_newTick;

	m_duration += m_tickDuration; //since start
}