/*
** All copyrights reserved by GridFury @ 2015 **
*/

#pragma once

#ifdef USE_CUSTOM_VECTOR_IMPLEMENTATION
class CDynMemoryPool
{
protected:
	Byte* m_memoryPool;
	UInt32 m_count;
	UInt32 m_size;
	UInt32 m_chunkSize;

protected:

	CDynMemoryPool( UInt32 chunkSize )
		: m_count( 0 )
		, m_size( 1 )
		, m_chunkSize( chunkSize )
	{
		m_memoryPool = reinterpret_cast< Byte* >( malloc( m_chunkSize ) );
	}

	void PrePush()
	{
		if ( m_size == m_count )
		{
			m_size *= 2;
		}

		++m_count;
	}

	void Reserve( UInt32 size )
	{
		m_memoryPool = reinterpret_cast< Byte* >( realloc( m_memoryPool, size * m_chunkSize ) );
		m_size = size;
	}
};

template<typename DataType>
class CRawIterator : public std::iterator<std::random_access_iterator_tag,
	DataType,
	Int64,
	DataType*,
	DataType&>
{
public:

	CRawIterator( DataType* ptr = nullptr ) { m_ptr = ptr; }
	CRawIterator( const CRawIterator<DataType>& rawIterator ) = default;
	~CRawIterator() {}

	CRawIterator<DataType>&					operator=( const CRawIterator<DataType>& rawIterator ) = default;
	CRawIterator<DataType>&					operator=( DataType* ptr ) { m_ptr = ptr;return ( *this ); }

	operator bool()const
	{
		if ( m_ptr )
			return true;
		else
			return false;
	}

	Bool                                        operator==( const CRawIterator<DataType>& rawIterator )const { return ( m_ptr == rawIterator.getConstPtr() ); }
	Bool                                        operator!=( const CRawIterator<DataType>& rawIterator )const { return ( m_ptr != rawIterator.getConstPtr() ); }

	CRawIterator<DataType>&						operator+=( const Int64& movement ) { m_ptr += movement;return ( *this ); }
	CRawIterator<DataType>&						operator-=( const Int64& movement ) { m_ptr -= movement;return ( *this ); }

	CRawIterator<DataType>&						operator++() { ++m_ptr;return ( *this ); }
	CRawIterator<DataType>&						operator--() { --m_ptr;return ( *this ); }

	//CRawIterator<DataType>						operator++() { auto temp( *this );++m_ptr;return temp; }
	//CRawIterator<DataType>						operator--() { auto temp( *this );--m_ptr;return temp; }

	CRawIterator<DataType>						operator+( const Int64& movement ) { auto oldPtr = m_ptr;m_ptr += movement;auto temp( *this );m_ptr = oldPtr;return temp; }
	CRawIterator<DataType>						operator-( const Int64& movement ) { auto oldPtr = m_ptr;m_ptr -= movement;auto temp( *this );m_ptr = oldPtr;return temp; }

	Int64										operator-( const CRawIterator<DataType>& rawIterator ) { return std::distance( rawIterator.getPtr(), this->getPtr() ); }

	DataType&									operator*() { return *m_ptr; }
	const DataType&								operator*()const { return *m_ptr; }
	DataType*									operator->() { return m_ptr; }

	DataType*									getPtr()const { return m_ptr; }
	const DataType*								getConstPtr()const { return m_ptr; }

protected:

	DataType*                                 m_ptr;
};

template<typename ItemsType>
class TDynArray : CDynMemoryPool
{
public:
	typedef CRawIterator<ItemsType> iterator;

private:
	ItemsType* m_objectsPool;

public:

	TDynArray()
		: CDynMemoryPool( sizeof( ItemsType ) )
	{
		m_objectsPool = ( ItemsType* )m_memoryPool;
	}

	void PushBack( ItemsType item )
	{
		PrePush();
		*( m_objectsPool + m_count - 1 ) = item;
	}

	iterator begin()
	{
		return iterator( m_objectsPool );
	}
};
#else
#include <vector>

template< typename ItemsType >
class TDynArray : public std::vector< ItemsType >
{ };
#endif