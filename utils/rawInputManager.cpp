/*
** All copyrights reserved by GridFury @ 2015 **
*/

#include "pch.h"

#include "rawInputManager.h"
#include "window.h"
#include "windowsInclude.h"

CRawInputManager::CRawInputManager()
{
}

CRawInputManager::~CRawInputManager()
{
}

void CRawInputManager::InputProc( Uint32 message, Uint32 parameter )
{
	if ( !m_currentInputContext )
		return;

	m_inputInfo.eventType = ( ERIEventType )message;

	if ( m_inputInfo.eventType == ERIEventType::RIE_MOUSEMOVE )
	{
		static POINTS points;
		points = MAKEPOINTS( parameter ); //HACK - shouldn't use WinAPI

		m_inputInfo.x = points.x;
		m_inputInfo.y = points.y;
	}
	else
	{
		m_inputInfo.x = parameter;
	}

	m_currentInputContext->ProcessInput( m_inputInfo );
}
