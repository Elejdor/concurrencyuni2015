/*
** All copyrights reserved by GridFury @ 2015 **
*/

#pragma once

extern void SRandXSAdd( UInt64 seed[2] );
extern void SRandXSAdd( UInt64 seed );
extern UInt64 RandXSAdd();