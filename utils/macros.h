/*
** All copyrights reserved by GridFury @ 2015 **
*/

#pragma once

#if defined(_DEBUG) || defined(DEBUG)
#define GF_BREAK() __debugbreak()
#else
#define GF_BREAK()
#endif

#define GF_INLINE inline
#define GF_FORCEINLINE __forceinline