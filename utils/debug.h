/*
** All copyrights reserved by GridFury @ 2015 **
*/

#pragma once

//#define NO_LOGGING
#if !defined(NO_LOGGING)
#define DEBUG_LEVEL 1 // 0 - break at info, 4 - never break
#include <cstdio>

namespace Debug
{
	extern void ShowMessageBox( Char*, Char* );

	//Use macros instead of it
	class SCDebug
	{
	public:
		template<typename... Args>
		static void Info( char* messageFormat, Args... args )
		{
			std::printf( messageFormat, args... );
		}

		template<typename... Args>
		static void Warning( const Char* messageFormat, Args... args )
		{
			std::printf( "Warning: " );
			std::printf( messageFormat, args... );

			if ( DEBUG_LEVEL < 2 )
			{
				GF_BREAK();
			}
		}

		template<typename... Args>
		static void Error( const Char* messageFormat, Args... args )
		{
			std::printf( "Error: " );
			std::printf( messageFormat, args... );
			if ( DEBUG_LEVEL < 3 )
			{
				GF_BREAK();
			}
		}

		template<typename... Args>
		static void Fatal( const Char* messageFormat, Args... args )
		{			
			std::printf( "Fatal error: " );
			std::printf( messageFormat, args... );
			if ( DEBUG_LEVEL < 4 )
			{
				GF_BREAK();
			}
		}
	};
}

#define BREAK_AT_LEVEL(x) if ( DEBUG_LEVEL <= x ) GF_BREAK()

#define DEBUG_INFO(...) { Debug::SCDebug::Info( __VA_ARGS__ ); BREAK_AT_LEVEL(0); }
#define DEBUG_WARNING(...) { Debug::SCDebug::Warning( __VA_ARGS__ ); BREAK_AT_LEVEL(1); }
#define DEBUG_ERROR(...) { Debug::SCDebug::Error( __VA_ARGS__ ); BREAK_AT_LEVEL(2); }
#define DEBUG_FATAL(...) { Debug::SCDebug::Fatal( __VA_ARGS__ ); BREAK_AT_LEVEL(3); }

#else

#define DEBUG_INFO(x)
#define DEBUG_WARNING(x)
#define DEBUG_ERROR(x)
#define DEBUG_FATAL(x)

#endif