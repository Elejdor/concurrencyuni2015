/*
** All copyrights reserved by GridFury @ 2015 **
*/

#include "pch.h"

#include "random.h"

UInt64 seedXSadd[2];

void SRandXSAdd( UInt64 seed[2] )
{
	seedXSadd[0] = seed[0];
	seedXSadd[1] = seed[1];
}

void SRandXSAdd( UInt64 seed )
{
	Uint32* pSeed = ( Uint32* )&seed;

	seedXSadd[0] = *pSeed;
	seedXSadd[1] = *( pSeed + 1 );
}

UInt64 RandXSAdd()
{
	UInt64 x = seedXSadd[0];
	const UInt64 y = seedXSadd[1];
	seedXSadd[0] = y;
	x ^= x << 23;
	x ^= x >> 17;
	x ^= y ^ ( y >> 26 );
	seedXSadd[1] = x;

	return x + y;
}