/*
** All copyrights reserved by GridFury @ 2015 **
*/

#pragma once

struct SInputInfo;

class IInputContext
{
public:
	virtual void ProcessInput( const SInputInfo& inputInfo ) = 0;
};