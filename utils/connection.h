#pragma once
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <stdio.h>

#pragma comment(lib, "Ws2_32.lib")

#define DEFAULT_PORT "27015"

#define ERROR_CHECK(x) if ( Int32 err = x ) return err;
#define RESULTMOD(x) x % 0x100

#define FLAG(x) 1 << x
#define GET_BIT(b, val) (!(val << b)) & 1

enum EOperations
{
	RandByte = FLAG( 0 ),
	RandBool = FLAG( 1 )
};

struct SQuery
{
	Char	m_operationFlag : 2;
	Char	m_resourceNum : 6;
};

struct SResponse
{
	Byte	m_data;
};

extern Int32 InitializeWinsoc();