#pragma once

template< typename T >
class TDynArray
{
private:
	T* m_ptr;
	Uint32 m_count;
	Uint32 m_size;

public:
	typedef T* iterator;

	TDynArray()
		: m_ptr( nullptr )
		, m_count( 0 )
		, m_size( 0 )
	{ }

	void PushBack( T element )
	{
		if ( m_count >= m_size )
		{
			Reserve( 2 * m_size + 1 );
		}

		m_ptr[m_count++] = element;
	}

	void Reserve( Uint32 size )
	{
		if ( size > m_size )
		{
			m_ptr = ( T* )realloc( m_ptr, size * sizeof( T ) );
			m_size = size;
		}
	}

	void Remove( Uint32 index )
	{
		T* remove = &m_ptr[index];
		memmove( remove, remove + 1, m_size - index );
		--m_count;
	}

	GF_INLINE Uint32 Size()
	{
		return m_size;
	}

	GF_INLINE Uint32 Count()
	{
		return m_count;
	}

	iterator operator[]( Uint32 index )
	{
		return &m_ptr[index];
	}
	iterator begin()
	{
		return m_ptr;
	}

	iterator end()
	{
		return &m_ptr[m_count];
	}
};