#pragma once
#include <thread>
#include <mutex>
#include <queue>
#include <windows.h>
#include <atomic>
#include <condition_variable>
#include <cstdio>

struct SClient
{
private:
	static Int32 Id;
	Int32 m_id;

public:
	SClient()
		: m_cut(false)
	{
		m_id = Id++;
	}

	GF_INLINE void Cut()
	{
		printf("Client cut: %i\n", m_id);
	}

private:
	Bool m_cut;
};

class CBarberShop
{
private:
	std::thread				m_barberThread;
	std::queue<SClient*>	m_waitingRoom;

	typedef std::lock_guard<std::mutex> tMutexLock;
	std::mutex				m_waitingMtx;
	std::condition_variable m_cvClientsWaiting;
	Bool					m_quit;

public:
	CBarberShop();

	void Init();
	void Shutdown();
	void AddClient(SClient* client);

private:
	void Cut(SClient* client);
	void Barber();
};