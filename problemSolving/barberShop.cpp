#include "pch.h"
#include "barberShop.h"

Int32 SClient::Id = 0;

CBarberShop::CBarberShop() : m_quit(false)
{

}

void CBarberShop::Init()
{
	m_barberThread = std::thread([this] { this->Barber(); });
}

void CBarberShop::Shutdown()
{
	m_quit = true;
	m_barberThread.join();
}

void CBarberShop::AddClient(SClient* client)
{
	tMutexLock lock(m_waitingMtx);
	m_waitingRoom.push(client);
	m_cvClientsWaiting.notify_one();
	printf("Clients in waiting room: %i\n", m_waitingRoom.size());
}

void CBarberShop::Cut(SClient* client)
{
	Sleep(2000);
	client->Cut();
}

void CBarberShop::Barber()
{
	while (true)
	{
		std::mutex m_barberSleep;
		std::unique_lock<std::mutex> lk(m_barberSleep);
		m_cvClientsWaiting.wait(lk, [&] { return !m_waitingRoom.empty() || m_quit; });

		if (m_quit && m_waitingRoom.empty())
		{
			break;
		}

		SClient* currentClient = nullptr;
		{
			tMutexLock lk2(m_waitingMtx);
			currentClient = m_waitingRoom.front();
			m_waitingRoom.pop();
		}
		Cut(currentClient);
		delete currentClient;

		lk.unlock();
	}
}
