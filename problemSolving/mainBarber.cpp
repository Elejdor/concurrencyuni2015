#include "pch.h"
#include "barberShop.h"

int main()
{
	CBarberShop barberShop;
	barberShop.Init();

	SClient* clientPtr;

	clientPtr = new SClient;
	barberShop.AddClient(clientPtr);

	Sleep(3000);
	clientPtr = new SClient;
	barberShop.AddClient(clientPtr);

	clientPtr = new SClient;
	barberShop.AddClient(clientPtr);

	barberShop.Shutdown();
	return 0;
}