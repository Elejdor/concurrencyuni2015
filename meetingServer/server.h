#pragma once
#include "../utils/dynArray.h"

class CQueryProcessor
{
	struct SResource
	{
		static Int32 Id;

	private:
		Int32 m_id;

	public:
		SResource();

		Byte RandByte();
		Byte RandBool();
	};
	
	typedef Byte ( SResource::*Operation ) ( );

private:
	TDynArray< Operation > m_operations;
	TDynArray< SResource > m_resources;

public:
	CQueryProcessor();

	SResponse ProcessQuery( SQuery query );
};

class CServer
{
private:
	SOCKET m_listenSocket;
	SOCKET m_clientSocket;

	struct addrinfo *m_result, *m_ptr;
	CQueryProcessor m_processor;

public:
	CServer();
	~CServer();
	Int32 Run();

private:
	void Respond( SQuery query );
	Byte Receive();

	Int32 CreateServerSocket();
	Int32 BindSocket();
	Int32 Listen();
	Int32 AcceptClient();
	Int32 SendAndReceive();
};