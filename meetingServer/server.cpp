#include "pch.h"
#include "server.h"
#include <random>

CServer::CServer()
	: m_result( nullptr )
	, m_ptr( nullptr )
{
	std::srand( 0 );
}

CServer::~CServer()
{
	shutdown( m_clientSocket, SD_SEND );
	closesocket( m_clientSocket );
	WSACleanup();
}

Int32 CServer::Run()
{
	ERROR_CHECK( InitializeWinsoc() );
	ERROR_CHECK( CreateServerSocket() );
	ERROR_CHECK( BindSocket() );
	freeaddrinfo( m_result );
	ERROR_CHECK( Listen() );
	ERROR_CHECK( AcceptClient() );
	ERROR_CHECK( SendAndReceive() );
}

void CServer::Respond( SQuery query )
{
	SResponse response = m_processor.ProcessQuery( query );
	Byte size = sizeof( SResponse );
	send( m_clientSocket, ( char* )&response, size, 0 );
}

Byte CServer::Receive()
{
	const Int32 recvbuflen = sizeof(SQuery);
	char recvbuf[recvbuflen];

	recv( m_clientSocket, recvbuf, recvbuflen, 0 );

	return *recvbuf;
}

Int32 CServer::CreateServerSocket()
{
	m_result = NULL;
	m_ptr = NULL;
	addrinfo hints;

	ZeroMemory( &hints, sizeof( hints ) );
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	Int32 errCode;

	// Resolve the local address and port to be used by the server
	errCode = getaddrinfo( NULL, DEFAULT_PORT, &hints, &m_result );
	if ( errCode != 0 ) {
		printf( "getaddrinfo failed: %d\n", errCode );
		WSACleanup();
		return 1;
	}

	m_listenSocket = INVALID_SOCKET;

	// Create a SOCKET for the server to listen for client connections
	m_listenSocket = socket( m_result->ai_family, m_result->ai_socktype, m_result->ai_protocol );

	if ( m_listenSocket == INVALID_SOCKET ) {
		printf( "Error at socket(): %ld\n", WSAGetLastError() );
		freeaddrinfo( m_result );
		WSACleanup();
		return 1;
	}

	return 0;
}

Int32 CServer::BindSocket()
{
	Int32 errorCode;

	// Setup the TCP listening socket
	errorCode = bind( m_listenSocket, m_result->ai_addr, ( int )m_result->ai_addrlen );
	if ( errorCode == SOCKET_ERROR ) {
		printf( "bind failed with error: %d\n", WSAGetLastError() );
		freeaddrinfo( m_result );
		closesocket( m_listenSocket );
		WSACleanup();
		return 1;
	}

	return 0;
}

Int32 CServer::Listen()
{
	if ( listen( m_listenSocket, SOMAXCONN ) == SOCKET_ERROR ) {
		printf( "Listen failed with error: %ld\n", WSAGetLastError() );
		closesocket( m_listenSocket );
		WSACleanup();
		return 1;
	}

	return 0;
}

Int32 CServer::AcceptClient()
{
	m_clientSocket = INVALID_SOCKET;

	// Accept a client socket
	m_clientSocket = accept( m_listenSocket, NULL, NULL );
	if ( m_clientSocket == INVALID_SOCKET ) {
		printf( "accept failed: %d\n", WSAGetLastError() );
		closesocket( m_listenSocket );
		WSACleanup();
		return 1;
	}

	return 0;
}

Int32 CServer::SendAndReceive()
{

	int iResult, iSendResult;
	CQueryProcessor processor;
	Byte size = sizeof( SQuery );

	Byte message;
	SQuery* query = nullptr;

	message = Receive();
	query = (SQuery*)&message;
	Respond( *query );

	message = Receive();
	query = ( SQuery* )&message;
	Respond( *query );

	message = Receive();
	query = ( SQuery* )&message;
	Respond( *query );

	closesocket( m_clientSocket );
	WSACleanup();

	return 0;
}

CQueryProcessor::CQueryProcessor()
{
	// Map methods
	m_operations.PushBack( &SResource::RandByte );
	m_operations.PushBack( &SResource::RandBool );

	SResource* tmpRes = new SResource;
	m_resources.PushBack( *tmpRes );
	delete tmpRes;
	tmpRes = nullptr;

	tmpRes = new SResource;
	m_resources.PushBack( *tmpRes );
	delete tmpRes;
	tmpRes = nullptr;
}

SResponse CQueryProcessor::ProcessQuery( SQuery query )
{
	Sleep( 2000 );

	const Uint32 opCount = m_operations.Count();
	const Uint32 resCount = m_resources.Count();

	Byte result;
	for ( Byte i = 0; i < opCount ; ++i )
	{
		if ( ( Byte )query.m_operationFlag & FLAG( i ) )
		{
			for ( Byte j = 0; j < resCount; ++j )
			{
				if ( FLAG( j ) & query.m_resourceNum )
				{
					SResource* res = m_resources[j];
					result = ( res->*( *m_operations[i] ) )( );
				}
			}

		}
	}
	
	Byte* tmpCopy = new Byte[ sizeof(SResponse) ];
	( *tmpCopy ) = result;

	SResponse response = *reinterpret_cast< SResponse* >( tmpCopy );
	delete tmpCopy;

	return response;
}

Int32 CQueryProcessor::SResource::Id = 0;

CQueryProcessor::SResource::SResource()
{
	m_id = Id++;
}

Byte CQueryProcessor::SResource::RandByte()
{
	printf( "Rand byte, resource: %i\n", m_id );
	return RESULTMOD( std::rand() );
}

Byte CQueryProcessor::SResource::RandBool()
{
	printf( "Rand bool, resource: %i\n", m_id );
	return std::rand() % 2;
}
