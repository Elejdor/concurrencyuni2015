#include "pch.h"
#include "application.h"

#include "../utils/rawInputManager.h"
#include "../utils/windowsInclude.h"
#include "../utils/window.h"
#include "../utils/debug.h"

extern CApp* CreateApplication();

CApp* GApplication = CreateApplication();

void ForwardWindowInput( Uint32 message, Uint32 wParam )
{
	GApplication->GetInput()->InputProc( message, wParam );
}

CApp::CApp()
	: m_quit( false )
	, m_inputContext( nullptr )
	, m_input( nullptr )
{
}

CApp::~CApp()
{

}

void CApp::Init()
{
	m_input = new CRawInputManager;

	if ( m_inputContext )
	{
		m_input->SetInputContext( m_inputContext );
	}
	else
	{
		DEBUG_ERROR( "Input context uninitialized.\n" );
	}

	m_window = new CWindow;
	m_window->Create();
}

void CApp::MainLoop()
{
	MSG msg;
	while ( true )
	{
		while ( PeekMessage( &msg, reinterpret_cast< HWND >( m_window->GetHwnd() ), 0, 0, PM_REMOVE ) )
		{
			TranslateMessage( &msg );
			DispatchMessage( &msg );
		}

		if ( GApplication->Quit() )
			break;

		Tick();
	}
}

void CApp::Shutdown()
{
	delete m_window;
	m_window = nullptr;

	delete m_input;
	m_input = nullptr;
}
