#pragma once
#include <atomic>
#include <mutex>

class CSpinLockMutex
{
private:
	std::atomic_flag m_flag;

public:
	CSpinLockMutex() _NOEXCEPT
	{
		m_flag.clear( std::memory_order_release );
	}

	void lock( )
	{
		while ( m_flag.test_and_set( std::memory_order_acquire ) );
	}

	void unlock()
	{
		m_flag.clear( std::memory_order_release );
	}

	CSpinLockMutex( const CSpinLockMutex& ) = delete;
	CSpinLockMutex& operator=( const CSpinLockMutex& ) = delete;
};

typedef std::lock_guard<CSpinLockMutex> SpinLockGuard;
