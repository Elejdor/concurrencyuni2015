#include "pch.h"
#include "jobManager.h"
#include "application.h"

CJobManager::CJobManager()
{
	for ( Byte i = 0; i < NUM_THREADS; ++i )
	{
		m_threads[i] = std::thread( [this, i]() -> void { this->ExecuteTasks( i ); } );
	}
}

CJobManager::~CJobManager()
{
	for ( Byte i = 0; i < NUM_THREADS; ++i )
	{
		m_threads[i].join();
	}
}

void CJobManager::ExecuteTasks( Byte threadId )
{
	while ( !GApplication->Quit() )
	{
		if ( m_jobs.empty() && !GApplication->Quit() )
		{
			if ( GApplication->Quit() )
				break;
		}
		else
		{
			IJob* currentJob = nullptr;

			{
				SpinLockGuard lock( m_jobsMutex );
				if ( !m_jobs.empty() )
				{
					currentJob = m_jobs.front();
					m_jobs.pop();
				}
			}

			if ( currentJob )
			{
				currentJob->Execute();
			}

		}
	}
}
