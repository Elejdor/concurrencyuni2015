#pragma once

class IJob
{
public:
	virtual void Execute() = 0;
};

template<class OwnerClass, typename Method>
class CJob : public IJob
{
private:
	OwnerClass m_owner;
	Method m_method;

public:
	CJob( OwnerClass owner, Method method )
		: m_owner( owner )
		, m_method( method )
	{
	}

	void Execute() override
	{
		( m_owner->*m_method )();
	}
};

class CSimpleJob : public IJob
{
private:
	void( *m_func )( );

public:
	CSimpleJob( void( *func )( ) )
		: m_func( func )
	{
	}

	virtual void Execute() override
	{
		m_func();
	}
};