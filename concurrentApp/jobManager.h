#pragma once
#include <queue>
#include <thread>
#include <mutex>

#include "spinLockMutex.h"
#include "job.h"

#define NUM_THREADS 4

class IJob;

class CJobManager
{
private:
	std::thread			m_threads[NUM_THREADS];
	std::queue<IJob*>	m_jobs;
	CSpinLockMutex		m_jobsMutex;

public:
	CJobManager();

	~CJobManager();

	template<class OwnerClass, typename Method>
	void AddAsyncMethod( OwnerClass owner, Method method ) 
	{
		SpinLockGuard lock( m_jobsMutex );
		m_jobs.push( new CJob<OwnerClass, Method>( owner, method ) );
	}

	template<typename Function>
	void AddAsyncFunction( Function func )
	{
		SpinLockGuard lock( m_jobsMutex );
		m_jobs.push( new CSimpleJob( func ) );
	}

private:

	void ExecuteTasks( Byte threadId );

};