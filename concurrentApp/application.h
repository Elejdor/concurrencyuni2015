#pragma once
#include "../utils/inputContext.h"

class CRawInputManager;
class CWindow;

class CApp
{
protected:
	IInputContext* m_inputContext;
	CRawInputManager* m_input;

	Bool m_quit;
	CWindow* m_window;
public:
	CApp();
	~CApp();

	GF_INLINE Bool Quit() const { return m_quit; }
	GF_INLINE CRawInputManager* const GetInput() const { return m_input; }
	GF_INLINE void RequestQuit() { m_quit = true; }

	virtual void Init();
	void MainLoop();
	virtual void Shutdown();

protected:
	virtual void Tick() = 0;
};

extern CApp* GApplication;